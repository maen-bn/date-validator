<?php


namespace Maenbn\DateValidator;

use DateTime;
use Exception;
use DateInterval;

class DateValidator
{
    /**
     * Check a user supplied date string is a historic date
     *
     * @param string $date
     *
     * @throws Exception
     *
     * @return bool
     */
    public static function isHistoric(string $date): bool
    {
        $userDate = DateTime::createFromFormat('d/m/Y', $date);
        if (! $userDate) {
            throw new Exception("Incorrect date format. Please use 'DD/MM/YYYY'");
        }
        $currentDate =  new DateTime();
        $diff = $userDate->diff($currentDate);

        return self::dayDifference($diff) > 0;
    }

    /**
     * Days in DateInterval doesn't show negative days i.e. days in the future. You have to check
     * if the interval property is 1 which means the days are actually negative
     *
     * @param DateInterval $diff
     *
     * @return false|int
     */
    private static function dayDifference(DateInterval $diff): int
    {
        $days = $diff->days;
        if ($diff->invert === 1) {
            $days = -$days;
        }
        return (int) $days;
    }
}
