# Date Validator

[![pipeline status](https://gitlab.com/maen-bn/date-validator/badges/master/pipeline.svg)](https://gitlab.com/maen-bn/date-validator/commits/master)

A very, very simple date validation PHP package. Only validates a date string to confirm if it's a historic date or not.

## Requirements

* PHP 7.3+

## Installation

Add the following to your `composer.json`

```json
"repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/maen-bn/date-validator.git"
    }
  ]
```

Then run `composer require maen-bn/date-validator:dev-master`

## Usage

The historic validation expects a date string in the format of "DD/MM/YYYY". If the format is incorrect, an Exception thrown. Otherwise, the validation returns true or false:

```php
try {
    if(DateValidator::isHistoric("01/01/2020")) {
    // Your successful code
    }   
 } catch (Exception $e) {
    // Handle error 
}
```

