<?php


namespace Tests;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use Maenbn\DateValidator\DateValidator;

class HistoricValidationTest extends TestCase
{
    public function testSuccessfulValidation()
    {
        $date = new DateTime('yesterday');
        $this->assertTrue(DateValidator::isHistoric($date->format('d/m/Y')));
    }

    public function testValidationFailure()
    {
        $date = new DateTime('today');
        $this->assertNotTrue(DateValidator::isHistoric($date->format('d/m/Y')));
        $tomorrow = new DateTime('tomorrow');
        $this->assertNotTrue(DateValidator::isHistoric($tomorrow->format('d/m/Y')));
    }

    public function testExceptionIsThrownWhenUserUsesAnIncorrectDateFormat()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Incorrect date format. Please use 'DD/MM/YYYY'");
        $date = new DateTime('yesterday');
        DateValidator::isHistoric($date->format('d-m-Y'));
    }
}
